package com.shopware.shopwareRestClient.util;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVWriter;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.shopware.shopwareRestClient.dto.transaction.LogTransactionDto;

public class CSVParser {

	private static final String DEFAULT_SEPARATOR = ";";
	private static Reader reader;
	private static Writer writer;
	private static CSVWriter csvWriter;

	public CSVParser(Path logfilePath) {
		try {
			writer = Files.newBufferedWriter(logfilePath);
			csvWriter = new CSVWriter(writer);
			reader = Files.newBufferedReader(logfilePath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void writeEntryToCsv(LogTransactionDto ltDto) {
		csvWriter.writeNext(new String[] { ltDto.getLoggedDate().toString(), ltDto.getLoggedEan(),
				ltDto.getLoggedAmount().toString(), ltDto.getLoggedArticleName(), ltDto.getLoggedArticleLink() });

		try {
			csvWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<LogTransactionDto> readCsvToEntryList(String file) {
		CsvToBean<LogTransactionDto> csvToBean = new CsvToBeanBuilder<LogTransactionDto>(reader)
				.withType(LogTransactionDto.class).withIgnoreLeadingWhiteSpace(true).build();

		Iterator<LogTransactionDto> logIterator = csvToBean.iterator();
		List<LogTransactionDto> ltdList = new ArrayList<>();
		while (logIterator.hasNext()) {
			LogTransactionDto ltd = logIterator.next();
			ltdList.add(ltd);
		}
		return ltdList;
	}

	public static void writeLine(String[] values) throws IOException {

		boolean first = true;

		StringBuilder sb = new StringBuilder();
		for (String value : values) {
			if (!first) {
				sb.append(DEFAULT_SEPARATOR);
			}
			sb.append(followCVSformat(value));
			first = false;
		}
		sb.append("\n");
		writer.append(sb.toString());
	}

	private static String followCVSformat(String value) {

		String result = value;
		if (result.contains("\"")) {
			result = result.replace("\"", "\"\"");
		}
		return result;

	}

	// public List<LogTransactionDto> readCsvToEntry(String file) {
	// List<LogTransactionDto> logEntryList = new ArrayList<>();
	// InputStream is = ClassLoader.class.getResourceAsStream(file);
	// BufferedReader in = new BufferedReader(new InputStreamReader(is));
	// String nextLine;
	// try {
	// while ((nextLine = in.readLine()) != null) {
	// LogTransactionDto logEntry = new LogTransactionDto();
	// StringTokenizer st = new StringTokenizer(nextLine, ";");
	// int counter = 0;
	// while (st.hasMoreTokens()) {
	// String rowCell = st.nextToken();
	// counter++;
	//
	// if (rowCell == null) {
	// break;
	// }
	//
	// if (counter == 1) {
	// logEntry.setLoggedDate(LocalDateTime.parse(rowCell));
	// }
	// if (counter == 2) {
	// logEntry.setLoggedEan(rowCell);
	// }
	// if (counter == 3) {
	// logEntry.setLoggedAmount(Integer.parseInt(rowCell));
	// }
	// if (counter == 4) {
	// logEntry.setLoggedArticleName(rowCell);
	// }
	// if (counter == 5) {
	// logEntry.setLoggedArticleLink(rowCell);
	// }
	// }
	//
	// if (logEntry.getLoggedEan() != null) {
	// logEntryList.add(logEntry);
	// }
	// }
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// return logEntryList;
	// }
	//
	// private List<String> listEntityKeywordsOfString(String rowCell) {
	// List<String> list = new ArrayList<>();
	//
	// StringTokenizer st = new StringTokenizer(rowCell, "|");
	// while (st.hasMoreTokens()) {
	// String item = st.nextToken();
	// list.add(item);
	// }
	// return list;
	// }
}