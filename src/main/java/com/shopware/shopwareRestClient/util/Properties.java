package com.shopware.shopwareRestClient.util;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class Properties {

	public static CompositeConfiguration load(String file) {	
		try {
			CompositeConfiguration config = new CompositeConfiguration();
			config.addConfiguration(new PropertiesConfiguration(file));
			return config;
		} catch (ConfigurationException e) {
		}
		return null;
	}	
}
