package com.shopware.shopwareRestClient;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.SystemConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Configuration {

	private static final Logger LOG = LoggerFactory.getLogger(Configuration.class);
	private static CompositeConfiguration config = new CompositeConfiguration();
	private static String uriToShopware;
	private static String mailUser;
	private static String mailPassword;
	private static String maintainerMail;

	public Configuration() {
		config.addConfiguration(new SystemConfiguration());
		try {
			config.addConfiguration(new PropertiesConfiguration("authenticationData.properties"));
		} catch (ConfigurationException e) {
			LOG.error(e.getMessage());
		}

		Configuration.uriToShopware = config.getString("uriToShopware");
		Configuration.mailUser = config.getString("mailUser");
		Configuration.mailPassword = config.getString("mailPassword");
		Configuration.maintainerMail = config.getString("maintainerMail");		
	}

	public static Map<String, String> getLoginData() {
		Map<String, String> loginData = new HashMap<String, String>();
		loginData.put("user", config.getString("shopwareUser"));
		loginData.put("password", config.getString("shopwarePassword"));
		return loginData;
	}

	public static String getUriToShopware() {
		return uriToShopware;
	}

	public static String getMailUser() {
		return mailUser;
	}

	public static String getMailPassword() {
		return mailPassword;
	}

	public static String getMaintainerMail() {
		return maintainerMail;
	}

	public static CompositeConfiguration getConfig() {
		return config;
	}

	public static void setConfig(CompositeConfiguration config) {
		Configuration.config = config;
	}

}
