package com.shopware.shopwareRestClient.fx;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import org.apache.commons.configuration.CompositeConfiguration;

import com.shopware.shopwareRestClient.dto.transaction.LogTransactionDto;
import com.shopware.shopwareRestClient.util.Properties;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class ViewLogsController implements Initializable {

	private ObservableList<LogTransactionDto> logs_data;
	private static ViewLogsController instance;

	@FXML
	private Tab loggingTab;
	@FXML
	private TableView<LogTransactionDto> logsTable;
	@FXML
	private TableColumn<LogTransactionDto, LocalDateTime> logsTableDateField;
	@FXML
	private TableColumn<LogTransactionDto, String> logsTableEanField;
	@FXML
	private TableColumn<LogTransactionDto, Integer> logsTableAmountField;
	@FXML
	private TableColumn<LogTransactionDto, String> logsTableArticleNameField;
	@FXML
	private TableColumn<LogTransactionDto, String> logsTableArticleLinkField;

	@FXML
	private VBox contentView_logs;

	/**
	 * initialize DataTable
	 */
	public void initialize(URL location, ResourceBundle resources) {
		// logsTableCounterField
		// .setCellValueFactory(new PropertyValueFactory<LogTransactionsDto,
		// Integer>("logsCounter"));
		// logsTableEanField.setCellValueFactory(new
		// PropertyValueFactory<LogTransactionsDto, String>("logsEan"));

		logs_data = FXCollections.observableArrayList();

		logsTable.setItems(logs_data);
		logsTable.setDisable(true);

		ViewLogsController.instance = this;
		loggingTab.setGraphic(ViewController.buildImage("/control/logs.png"));
	}

	public static ViewLogsController getInstance() {
		return instance;
	}

}