package com.shopware.shopwareRestClient.fx;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.configuration.CompositeConfiguration;

import com.shopware.shopwareRestClient.dto.article.BuySell.BuyingTableArticleDto;
import com.shopware.shopwareRestClient.dto.transaction.ArticleTransactionInfoDto;
import com.shopware.shopwareRestClient.services.ArticleTransaction;
import com.shopware.shopwareRestClient.services.ArticleTransactionImpl;
import com.shopware.shopwareRestClient.util.Properties;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ViewBuyController implements Initializable {

	private static final CompositeConfiguration statusMessages = Properties.load("status/de_status_msg.properties");

	private ObservableList<BuyingTableArticleDto> buy_data;
	private static String articleCode_buy;

	private static ViewBuyController instance;

	@FXML
	private Tab buyingTab;
	@FXML
	private ImageView productImageView_buy;
	@FXML
	private ComboBox<Integer> amount_buy;
	@FXML
	private Label statusField_buy;
	@FXML
	private Label articleNameField_buy;
	@FXML
	private Label articleStockField_buy;
	@FXML
	private Label sellingPriceField_buy;
	@FXML
	private Label eanField_buy;
	@FXML
	private Label longDescriptionField_buy;
	@FXML
	private Hyperlink articleShoplinkField_buy;

	@FXML
	private TextField codeField_buy;

	@FXML
	private TableView<BuyingTableArticleDto> buyTable;
	@FXML
	private TableColumn<BuyingTableArticleDto, Integer> buyTableCounterField;
	@FXML
	private TableColumn<BuyingTableArticleDto, String> buyTableEanField;

	@FXML
	private VBox contentView_buy;

	/**
	 * initialize DataTable
	 */
	public void initialize(URL location, ResourceBundle resources) {
		buyTableCounterField
				.setCellValueFactory(new PropertyValueFactory<BuyingTableArticleDto, Integer>("buyCounter"));
		buyTableEanField.setCellValueFactory(new PropertyValueFactory<BuyingTableArticleDto, String>("buyEan"));

		buy_data = FXCollections.observableArrayList();

		buyTable.setItems(buy_data);

		amount_buy.getItems().setAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
		amount_buy.getSelectionModel().select(0);

		ViewBuyController.instance = this;
		buyingTab.setGraphic(ViewController.buildImage("/control/buy.png"));
	}

	@FXML
	public void amountBuyChange() {
		codeField_buy.requestFocus();
	}

	@FXML
	public void handleBuyInputFocus() {
		Platform.runLater(() -> {
			if (!codeField_buy.isFocused()) {
				codeField_buy.requestFocus();
				handleBuyInputFocus();
			}
		});
	}

	@FXML
	public void handleBuyingCodeEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			ViewController.getInstance().setProgressBar(0.1);
			String enteredCode = codeField_buy.getText();
			Integer amount = 1;
			try {
				amount = amount_buy.getSelectionModel().getSelectedItem();
			} catch (Exception e) {
				amount = 1;
			}
			final Integer finalAmount = amount;
			resetBuyMaskCleaner();
			ViewController.getInstance().setProgressBar(0.2);

			Thread thread = new Thread() {
				public void run() {
					ArticleTransaction transaction = new ArticleTransactionImpl();
					final ArticleTransactionInfoDto infoDto = transaction.articleTransaction(enteredCode, false,
							finalAmount);

					Platform.runLater(new Runnable() {
						public void run() {
							contentView_buy
									.setStyle("-fx-background-color:" + infoDto.getStatusMsg().getUiColour() + ";");
							statusField_buy.setText(ViewBuyController.statusMessages
									.getString(infoDto.getStatusMsg().getStatusMessage()));
							statusField_buy.setFont(Font.font("Verdana", FontWeight.BOLD, 14));

							ViewController.getInstance().updateProgressStatus(0.9);
							switch (infoDto.getStatusMsg()) {
							case TxtNotFound:
							case ArticleCodeEmpty:
							case ArticleNotFetched:
							case ArticleNumbersWrong:
							case ArticleResponseNotMappable:
							case ShopNotResolvable:
							case ArticleNotUpdated:
								ViewController.getInstance().supportWindow_btn.setDisable(false);
								ViewController.setSupportwindow(true);
								ViewController.setErrorSubject("ScannerManager Error Alert");
								ViewController.setErrorBody(ViewBuyController.statusMessages.getString(
										infoDto.getStatusMsg().getStatusMessage()) + " --> " + infoDto.toString());
								java.awt.Toolkit.getDefaultToolkit().beep();
								break;
							case TxtSuccess:
								ViewController.setSupportwindow(false);
								articleCode_buy = enteredCode;
								statusField_buy.setText("Erfolgreicher Zugang: " + enteredCode);
								articleNameField_buy.setText("Artikel: " + infoDto.getArticleName());
								articleStockField_buy
										.setText("Anzahl: " + Integer.toString(infoDto.getArticleStock()) + " Stück");
								sellingPriceField_buy.setText("Preis: " + infoDto.getSellingPrice() + " €");
								eanField_buy.setText("EAN: " + infoDto.getEan());
								longDescriptionField_buy.setText("Beschreibung: \n" + infoDto.getLongDescription());
								if (infoDto.getImageUrl() != null) {
									productImageView_buy.setImage(new Image(infoDto.getImageUrl()));
								} else {
									productImageView_buy.setImage(
											new Image(this.getClass().getResourceAsStream("/defaultArticle.png")));
								}
								articleShoplinkField_buy.setText("zum Artikel im Shop");

								// START: Fill Data-Table
								BuyingTableArticleDto buyingTableArticleDto = new BuyingTableArticleDto();

								boolean matching = false;
								int stadCounter = finalAmount;
								for (BuyingTableArticleDto stad : buy_data) {
									if (stad.getBuyEan().equals(infoDto.getEan())) {
										stadCounter = stad.getBuyCounter() + finalAmount;
										buy_data.remove(stad);
										break;
									}
								}

								if (matching == false) {
									buyingTableArticleDto.buyCounter.setValue(stadCounter);
									buyingTableArticleDto.buyEan.setValue(infoDto.getEan());
								}
								buy_data.add(buyingTableArticleDto);
								// END: Fill Data-Table

								ViewStatisticController.instance.updateBuyPieChart(finalAmount);

								buyTable.getColumns().get(0).setVisible(false);
								buyTable.getColumns().get(0).setVisible(true);
								break;
							}

							articleShoplinkField_buy.setOnAction(ev -> {
								try {
									Desktop.getDesktop().browse(URI.create(
											"https://www.maschebeimasche.com/shopware.php?sViewport=searchFuzzy&sLanguage=1&sSearch="
													+ articleCode_buy));
									codeField_buy.requestFocus();
								} catch (IOException e) {
									e.printStackTrace();
								}
							});
							ViewController.getInstance().updateProgressStatus(1.0);
						}
					});
				}
			};
			thread.start();
		}
	}

	public void resetBuyMaskCleaner() {
		// Clear Text/Image/Styling
		codeField_buy.setText("");
		amount_buy.getSelectionModel().select(0);
		productImageView_buy.setImage(null);
		statusField_buy.setText("");
		articleNameField_buy.setText("");
		articleStockField_buy.setText("");
		sellingPriceField_buy.setText("");
		eanField_buy.setText("");
		longDescriptionField_buy.setText("");
		articleShoplinkField_buy.setText("");

		// Text-styling
		statusField_buy.setTextFill(Color.web("#000"));
		articleNameField_buy.setTextFill(Color.web("#000"));
		articleStockField_buy.setTextFill(Color.web("#000"));
		sellingPriceField_buy.setTextFill(Color.web("#000"));
		eanField_buy.setTextFill(Color.web("#000"));
		longDescriptionField_buy.setTextFill(Color.web("#000"));

		articleNameField_buy.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		statusField_buy.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		articleStockField_buy.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		sellingPriceField_buy.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		eanField_buy.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		longDescriptionField_buy.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));

		contentView_buy.setStyle("");
	}

	public static ViewBuyController getInstance() {
		return instance;
	}

}