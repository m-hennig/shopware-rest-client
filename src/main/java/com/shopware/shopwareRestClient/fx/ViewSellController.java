package com.shopware.shopwareRestClient.fx;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ResourceBundle;

import org.apache.commons.configuration.CompositeConfiguration;

import com.shopware.shopwareRestClient.dto.article.BuySell.SellingTableArticleDto;
import com.shopware.shopwareRestClient.dto.transaction.ArticleTransactionInfoDto;
import com.shopware.shopwareRestClient.services.ArticleTransaction;
import com.shopware.shopwareRestClient.services.ArticleTransactionImpl;
import com.shopware.shopwareRestClient.util.Properties;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class ViewSellController implements Initializable {

	private static final CompositeConfiguration statusMessages = Properties.load("status/de_status_msg.properties");

	private ObservableList<SellingTableArticleDto> sell_data;
	private static String articleCode_sell;

	private static ViewSellController instance;

	@FXML
	private Tab sellingTab;
	@FXML
	private ImageView productImageView_sell;
	@FXML
	private ComboBox<Integer> amount_sell;
	@FXML
	private Label statusField_sell;
	@FXML
	private Label articleNameField_sell;
	@FXML
	private Label articleStockField_sell;
	@FXML
	private Label sellingPriceField_sell;
	@FXML
	private Label eanField_sell;
	@FXML
	private Label longDescriptionField_sell;
	@FXML
	private Hyperlink articleShoplinkField_sell;

	@FXML
	private TextField codeField_sell;

	@FXML
	private TableView<SellingTableArticleDto> sellTable;
	@FXML
	private TableColumn<SellingTableArticleDto, Integer> sellTableCounterField;
	@FXML
	private TableColumn<SellingTableArticleDto, String> sellTableEanField;

	@FXML
	private VBox contentView_sell;

	/**
	 * initialize DataTable
	 */
	public void initialize(URL location, ResourceBundle resources) {
		sellTableCounterField
				.setCellValueFactory(new PropertyValueFactory<SellingTableArticleDto, Integer>("sellCounter"));
		sellTableEanField.setCellValueFactory(new PropertyValueFactory<SellingTableArticleDto, String>("sellEan"));

		sell_data = FXCollections.observableArrayList();

		sellTable.setItems(sell_data);

		amount_sell.getItems().setAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);
		amount_sell.getSelectionModel().select(0);

		ViewSellController.instance = this;
		sellingTab.setGraphic(ViewController.buildImage("/control/sell.png"));
	}

	@FXML
	public void amountSellChange() {
		handleSellInputFocus();
	}

	@FXML
	public void handleSellInputFocus() {
		codeField_sell.requestFocus();
		Platform.runLater(() -> {
			if (!codeField_sell.isFocused()) {
				codeField_sell.requestFocus();
				handleSellInputFocus();
			}
		});
	}

	@FXML
	public void handleSellingCodeEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			ViewController.getInstance().setProgressBar(0.1);
			final String enteredCode = codeField_sell.getText();
			Integer amount = 1;
			try {
				amount = amount_sell.getSelectionModel().getSelectedItem();
			} catch (Exception e) {
				amount = 1;
			}
			final Integer finalAmount = amount;
			resetSellMaskCleaner();
			ViewController.getInstance().setProgressBar(0.2);

			Thread thread = new Thread() {
				public void run() {
					ArticleTransaction transaction = new ArticleTransactionImpl();
					final ArticleTransactionInfoDto infoDto = transaction.articleTransaction(enteredCode, true,
							finalAmount);
					Platform.runLater(new Runnable() {
						public void run() {
							contentView_sell
									.setStyle("-fx-background-color:" + infoDto.getStatusMsg().getUiColour() + ";");
							statusField_sell.setText(ViewSellController.statusMessages
									.getString(infoDto.getStatusMsg().getStatusMessage()));
							statusField_sell.setFont(Font.font("Verdana", FontWeight.BOLD, 14));

							ViewController.getInstance().updateProgressStatus(0.9);
							switch (infoDto.getStatusMsg()) {
							case TxtNotFound:
							case ArticleCodeEmpty:
							case ArticleNotFetched:
							case ArticleNumbersWrong:
							case ArticleResponseNotMappable:
							case ShopNotResolvable:
							case ArticleNotUpdated:
								ViewController.getInstance().supportWindow_btn.setDisable(false);
								ViewController.setSupportwindow(true);
								ViewController.setErrorSubject("ScannerManager Error Alert");
								ViewController.setErrorBody(ViewSellController.statusMessages.getString(
										infoDto.getStatusMsg().getStatusMessage()) + " --> " + infoDto.toString());
								java.awt.Toolkit.getDefaultToolkit().beep();
								break;
							case TxtSuccess:
								ViewController.setSupportwindow(false);
								articleCode_sell = enteredCode;
								statusField_sell.setText("Erfolgreich verkauft: " + enteredCode);
								articleNameField_sell.setText("Artikel: " + infoDto.getArticleName());
								articleStockField_sell
										.setText("Anzahl: " + Integer.toString(infoDto.getArticleStock()) + " Stück");
								sellingPriceField_sell.setText("Preis: " + infoDto.getSellingPrice() + " €");
								eanField_sell.setText("EAN: " + infoDto.getEan());
								longDescriptionField_sell.setText("Beschreibung: \n" + infoDto.getLongDescription());
								if (infoDto.getImageUrl() != null) {
									productImageView_sell.setImage(new Image(infoDto.getImageUrl()));
								} else {
									productImageView_sell.setImage(
											new Image(this.getClass().getResourceAsStream("/defaultArticle.png")));
								}
								articleShoplinkField_sell.setText("zum Artikel im Shop");

								// START: Fill Data-Table
								SellingTableArticleDto sellingTableArticleDto = new SellingTableArticleDto();

								boolean matching = false;
								int stadCounter = finalAmount;
								for (SellingTableArticleDto stad : sell_data) {
									if (stad.getSellEan().equals(infoDto.getEan())) {
										stadCounter = stad.getSellCounter() + finalAmount;
										sell_data.remove(stad);
										break;
									}
								}

								if (matching == false) {
									sellingTableArticleDto.sellCounter.setValue(stadCounter);
									sellingTableArticleDto.sellEan.setValue(infoDto.getEan());
								}
								sell_data.add(sellingTableArticleDto);
								// END: Fill Data-Table

								ViewStatisticController.instance.updateSellPieChart(finalAmount);

								sellTable.getColumns().get(0).setVisible(false);
								sellTable.getColumns().get(0).setVisible(true);
								break;
							}

							articleShoplinkField_sell.setOnAction(ev -> {
								try {
									Desktop.getDesktop().browse(URI.create(
											"https://www.maschebeimasche.com/shopware.php?sViewport=searchFuzzy&sLanguage=1&sSearch="
													+ articleCode_sell));
									codeField_sell.requestFocus();
								} catch (IOException e) {
									e.printStackTrace();
								}
							});

							ViewController.getInstance().updateProgressStatus(1.0);
						}
					});
				}
			};
			thread.start();
		}

		blockUiDuringProcessing(false);
	}

	private void blockUiDuringProcessing(boolean needBlocking) {
		if (needBlocking) {
			// TODO start Blocking!
			codeField_sell.setDisable(true);
		} else {
			// TODO reverse Blocking!
			codeField_sell.setDisable(false);
		}
	}

	public void resetSellMaskCleaner() {
		// Clear Text/Image/Styling
		codeField_sell.setText("");
		amount_sell.getSelectionModel().select(0);
		productImageView_sell.setImage(null);
		statusField_sell.setText("");
		articleNameField_sell.setText("");
		articleStockField_sell.setText("");
		sellingPriceField_sell.setText("");
		eanField_sell.setText("");
		longDescriptionField_sell.setText("");
		articleShoplinkField_sell.setText("");

		// Text-styling
		statusField_sell.setTextFill(Color.web("#000"));
		articleNameField_sell.setTextFill(Color.web("#000"));
		articleStockField_sell.setTextFill(Color.web("#000"));
		sellingPriceField_sell.setTextFill(Color.web("#000"));
		eanField_sell.setTextFill(Color.web("#000"));
		longDescriptionField_sell.setTextFill(Color.web("#000"));

		articleNameField_sell.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		statusField_sell.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		articleStockField_sell.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		sellingPriceField_sell.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		eanField_sell.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));
		longDescriptionField_sell.setFont(Font.font("Verdana", FontWeight.NORMAL, 11));

		contentView_sell.setStyle("");
	}

	public static ViewSellController getInstance() {
		return instance;
	}

}