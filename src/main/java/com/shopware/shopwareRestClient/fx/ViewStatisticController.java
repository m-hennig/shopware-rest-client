package com.shopware.shopwareRestClient.fx;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Tab;

public class ViewStatisticController implements Initializable {

	public static ViewStatisticController instance; 
	private ObservableList<PieChart.Data> pieChartData;

	@FXML
	private Tab statisticTab;
	@FXML
	private PieChart piechart;

	/**
	 * initialize Chart
	 */
	public void initialize(URL location, ResourceBundle resources) {
		ViewStatisticController.instance = this;
		this.pieChartData = FXCollections.observableArrayList(
				new PieChart.Data("Verkauft", 0),
				new PieChart.Data("Eingegangen", 0));

		piechart.setTitle("Gegenüberstellung Verkauf/Eingang");
		piechart.setData(pieChartData);
		
		statisticTab.setGraphic(ViewController.buildImage("/control/statistic.png"));
	}

	public void updateSellPieChart(int amountToAdd) {
		updatePieChart(amountToAdd, 0);
	}
	
	public void updateBuyPieChart(int amountToAdd) {
		updatePieChart(amountToAdd, 1);
	}
	
	public void updatePieChart(int amountToAdd, int index) {
		this.pieChartData.get(index).setPieValue(pieChartData.get(index).getPieValue() + amountToAdd);		
	}
}