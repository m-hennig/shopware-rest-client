package com.shopware.shopwareRestClient.fx;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tab;

public class ViewConfigController implements Initializable {

	public static ViewConfigController instance; 

	@FXML
	private Tab configTab;
	
	@FXML
	private CheckBox config1box;
	
	@FXML
	private CheckBox config2box;
	
	@FXML
	private CheckBox config3box;
	
	@FXML
	private CheckBox config4box;
	
	@FXML
	private CheckBox config5box;
	
	@FXML
	private CheckBox config6box;
	
	@FXML
	private CheckBox config7box;
	
	@FXML
	private CheckBox config8box;
	
	@FXML
	private CheckBox config9box;
	
	@FXML
	private CheckBox config10box;
	
	@FXML
	private CheckBox config11box;
	
	@FXML
	private CheckBox config12box;
	
	/**
	 * initialize Chart
	 */
	public void initialize(URL location, ResourceBundle resources) {
		ViewConfigController.instance = this;
		configTab.setGraphic(ViewController.buildImage("/control/config3.png"));
	}

}