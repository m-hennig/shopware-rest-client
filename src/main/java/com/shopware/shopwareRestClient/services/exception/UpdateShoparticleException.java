package com.shopware.shopwareRestClient.services.exception;

public class UpdateShoparticleException extends Exception {

	private int statusCode;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UpdateShoparticleException(String message, int status) {
		super(message);
		setStatusCode(status);
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

}
