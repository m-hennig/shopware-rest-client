package com.shopware.shopwareRestClient.services;

import java.net.SocketException;
import javax.ws.rs.ProcessingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.shopware.shopwareRestClient.Configuration;
import com.shopware.shopwareRestClient.dto.article.Article;
import com.shopware.shopwareRestClient.dto.article.ArticleVariants;
import com.shopware.shopwareRestClient.dto.transaction.ArticleTransactionInfoDto;
import com.shopware.shopwareRestClient.fx.ViewController;
import com.shopware.shopwareRestClient.rest.ShopwareRestCallArticle;
import com.shopware.shopwareRestClient.services.exception.ArticleNotFoundException;
import com.shopware.shopwareRestClient.services.exception.UpdateShoparticleException;

public class ArticleTransactionImpl implements ArticleTransaction {

	private static final Logger LOG = LoggerFactory.getLogger(ArticleTransactionImpl.class);

	private ShopwareRestCallArticle articleCaller;
	private ArticleTransactionService articleController;

	public ArticleTransactionImpl() {
		articleController = new ArticleTransactionService();
		articleCaller = new ShopwareRestCallArticle(Configuration.getLoginData().get("user"),
				Configuration.getLoginData().get("password"));
	}

	public ArticleTransactionInfoDto articleTransaction(String codeInput, boolean isSell, Integer amount) {
		LOG.debug("articleTransaction() mit dem codeInput {} aufgerufen", codeInput);
		ArticleTransactionInfoDto dto = new ArticleTransactionInfoDto();
		if (codeInput.isEmpty()) {
			dto.setStatusMsg(StatusMessage.ArticleCodeEmpty);
			return dto;
		}

		ViewController.getInstance().updateProgressStatus(0.4);

		dto.setEan(codeInput);
		Article article = null;
		try {
			article = articleCaller.getArticleById(codeInput, true);
			ViewController.getInstance().updateProgressStatus(0.6);
			if (!new QualityGate().articleQualityCheck(article)) {
				dto.setStatusMsg(StatusMessage.ArticleNumbersWrong);
				return dto;
			}
			ViewController.getInstance().updateProgressStatus(0.7);
			if (article != null && article.getSuccess()) {
				try {
					dto = doTransaction(codeInput, isSell, article, amount, dto);
				} catch (UpdateShoparticleException e) {
					LOG.error("Error while updating Arctile! --> Statuscode: " + e.getStatusCode());
					dto.setStatusMsg(StatusMessage.ArticleNotUpdated);
				}
				dto.setStatusMsg(StatusMessage.TxtSuccess);
			} else {
				dto.setStatusMsg(StatusMessage.ArticleNotFetched);
				return dto;
			}
		} catch (JsonMappingException e) {
			LOG.error("Error in Mapping JSON to Objects -> " + e.getMessage());
			dto.setStatusMsg(StatusMessage.ArticleResponseNotMappable);
		} catch (SocketException e) {
			LOG.error("Error while creating socket: " + e.getMessage());
			dto.setStatusMsg(StatusMessage.ShopNotResolvable);
		} catch (ProcessingException e) {
			LOG.error("Error while resolving Host -> " + e.getMessage());
			dto.setStatusMsg(StatusMessage.ShopNotResolvable);
		}
		ViewController.getInstance().updateProgressStatus(0.8);
		return dto;
	}

	/**
	 * 
	 * @param codeInput
	 * @param isSell
	 * @param article
	 * @return
	 * @throws UpdateShoparticleException
	 */
	private ArticleTransactionInfoDto doTransaction(String codeInput, boolean isSell, Article article, Integer amount,
			ArticleTransactionInfoDto dto) throws UpdateShoparticleException {
		ArticleVariants scannedVariant = checkForVariant(codeInput, article);

		if (scannedVariant != null) {
			article = articleController.updateVariants(codeInput, article, isSell, amount);
			dto = articleController.sendVariants(codeInput, scannedVariant, true, article);
		} else {
			if (codeInput.equals(article.getData().getMainDetail().getNumber())
					|| codeInput.equals(article.getData().getMainDetail().getEan())) {
				articleController.updateArticles(codeInput, article, isSell, amount);
				dto = articleController.sendArticle(codeInput, article, false);
			}
		}
		return dto;
	}

	private ArticleVariants checkForVariant(String codeInput, Article article) {
		if (article != null && article.getData() != null && article.getData().getDetails() != null) {
			for (ArticleVariants variants : article.getData().getDetails()) {
				if (codeInput.equals(variants.getEan())) {
					return variants;
				}
			}
		}
		return null;
	}

	@Override
	public ArticleTransactionInfoDto searchArticle(String codeInput) {
		ArticleTransactionInfoDto infoDto = new ArticleTransactionInfoDto();
		Article article = null;
		try {
			article = articleCaller.getArticleById(codeInput, true);
			ViewController.getInstance().updateProgressStatus(0.6);
			ArticleVariants scannedVariant = checkForVariant(codeInput, article);

			if (scannedVariant != null) {
				infoDto = articleController.fillVariantsToInfoDto(scannedVariant, article);
			} else {
				if(article.getData() == null || article.getData().getMainDetail() == null || article.getData().getMainDetail().getInStock() == null) {
					throw new ArticleNotFoundException("Article or some parts of the article seem to be null!");
				}
				infoDto = articleController.fillArticleInfoDto(article.getData(),
						article.getData().getMainDetail().getInStock());
			}

			ViewController.getInstance().updateProgressStatus(0.8);
			infoDto.setStatusMsg(StatusMessage.TxtSuccess);

		} catch (JsonMappingException e) {
			LOG.error("Error in Mapping JSON to Objects -> " + e.getMessage());
			infoDto.setStatusMsg(StatusMessage.ArticleResponseNotMappable);
		} catch (SocketException e) {
			LOG.error("Error while creating socket: " + e.getMessage());
			infoDto.setStatusMsg(StatusMessage.ShopNotResolvable);
		} catch (ProcessingException e) {
			LOG.error("Error while resolving Host -> " + e.getMessage());
			infoDto.setStatusMsg(StatusMessage.ShopNotResolvable);
		} catch (ArticleNotFoundException e) {
			LOG.error("Error while resolving Article -> " + e.getMessage());
			infoDto.setStatusMsg(StatusMessage.TxtNotFound);
		}

		return infoDto;
	}
}