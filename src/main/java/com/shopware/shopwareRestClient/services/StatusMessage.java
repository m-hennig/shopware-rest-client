package com.shopware.shopwareRestClient.services;

public enum StatusMessage {

	TxtSuccess(200, "#c9ee82", "statusmsg1"),
	TxtNotFound(400, "#eea782", "errmsg1"),
	ArticleNumbersWrong(1500, "#eea782", "errmsg2"), 
	ArticleNotFetched(1401, "#eea782", "errmsg3"), 
	ArticleCodeEmpty(1400, "#eea782", "errmsg4"),
	ArticleResponseNotMappable(1600, "#eea782", "errmsg5"),
	ShopNotResolvable(1700, "#eea782", "errmsg6"), 
	ArticleNotUpdated(1800, "#eea782", "errmsg7");

	private int statusCode;
	private String uiColour;
	private String statusMessage;

	private StatusMessage(int statusCode, String uiColour, String statusMessage) {
		this.statusCode = statusCode;
		this.uiColour = uiColour;
		this.statusMessage = statusMessage;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getUiColour() {
		return uiColour;
	}

	public void setUiColour(String uiColour) {
		this.uiColour = uiColour;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

}
