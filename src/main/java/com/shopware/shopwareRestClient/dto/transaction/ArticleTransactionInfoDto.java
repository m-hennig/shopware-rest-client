package com.shopware.shopwareRestClient.dto.transaction;

import com.shopware.shopwareRestClient.services.StatusMessage;

public class ArticleTransactionInfoDto {

	private StatusMessage statusMsg;

	/**
	 * Image-URL (path) of GET api/media/mediaId
	 */
	private String imageUrl;
	private String articleName;
	private int articleStock;
	private double sellingPrice;
	private String ean;
	private String longDescription;

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getArticleName() {
		return articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}

	public int getArticleStock() {
		return articleStock;
	}

	public void setArticleStock(int articleStock) {
		this.articleStock = articleStock;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public String getLongDescription() {
		return longDescription;
	}

	public void setLongDescription(String longDescription) {
		this.longDescription = longDescription;
	}

	public StatusMessage getStatusMsg() {
		return statusMsg;
	}

	public void setStatusMsg(StatusMessage statusMsg) {
		this.statusMsg = statusMsg;
	}

	@Override
	public String toString() {
		return "ArticleTransactionInfoDto [statusMsg=" + statusMsg + ", imageUrl=" + imageUrl + ", articleName="
				+ articleName + ", articleStock=" + articleStock + ", sellingPrice=" + sellingPrice + ", ean=" + ean
				+ ", longDescription=" + longDescription + "]";
	}

}