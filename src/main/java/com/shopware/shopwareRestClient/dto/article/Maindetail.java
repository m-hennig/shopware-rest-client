package com.shopware.shopwareRestClient.dto.article;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({ "purchasePrice" })
public class Maindetail {

	private Integer id;
	private Integer articleId;
	private Integer unitId;

	/**
	 * Article Number (EAN for ScannerManager)
	 */
	private String number;

	private String supplierNumber;
	private Integer kind;
	private String additionalText;
	private boolean active;
	private Integer inStock;
	private Integer stockMin;
	private boolean lastStock;
	private String weight;
	private Object width;
	private Object len;
	private Object height;
	private String ean;
	private Integer purchasePrice;
	private Integer position;
	private Integer minPurchase;
	private Object purchaseSteps;
	private Object maxPurchase;
	private String purchaseUnit;
	private Object referenceUnit;
	private String packUnit;
	private Boolean shippingFree;
	private Object releaseDate;
	private String shippingTime;
	private Price[] prices;
	private Attribute attribute;
	private Object[] configuratorOptions;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getArticleId() {
		return articleId;
	}

	public void setArticleId(Integer articleId) {
		this.articleId = articleId;
	}

	public Integer getUnitId() {
		return unitId;
	}

	public void setUnitId(Integer unitId) {
		this.unitId = unitId;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getSupplierNumber() {
		return supplierNumber;
	}

	public void setSupplierNumber(String supplierNumber) {
		this.supplierNumber = supplierNumber;
	}

	public Integer getKind() {
		return kind;
	}

	public void setKind(Integer kind) {
		this.kind = kind;
	}

	public String getAdditionalText() {
		return additionalText;
	}

	public void setAdditionalText(String additionalText) {
		this.additionalText = additionalText;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Integer getInStock() {
		return inStock;
	}

	public void setInStock(Integer inStock) {
		this.inStock = inStock;
	}

	public Integer getStockMin() {
		return stockMin;
	}

	public void setStockMin(Integer stockMin) {
		this.stockMin = stockMin;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public Object getWidth() {
		return width;
	}

	public void setWidth(Object width) {
		this.width = width;
	}

	public Object getLen() {
		return len;
	}

	public void setLen(Object len) {
		this.len = len;
	}

	public Object getHeight() {
		return height;
	}

	public void setHeight(Object height) {
		this.height = height;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getMinPurchase() {
		return minPurchase;
	}

	public void setMinPurchase(Integer minPurchase) {
		this.minPurchase = minPurchase;
	}

	public Object getPurchaseSteps() {
		return purchaseSteps;
	}

	public void setPurchaseSteps(Object purchaseSteps) {
		this.purchaseSteps = purchaseSteps;
	}

	public Object getMaxPurchase() {
		return maxPurchase;
	}

	public void setMaxPurchase(Object maxPurchase) {
		this.maxPurchase = maxPurchase;
	}

	public String getPurchaseUnit() {
		return purchaseUnit;
	}

	public void setPurchaseUnit(String purchaseUnit) {
		this.purchaseUnit = purchaseUnit;
	}

	public Object getReferenceUnit() {
		return referenceUnit;
	}

	public void setReferenceUnit(Object referenceUnit) {
		this.referenceUnit = referenceUnit;
	}

	public String getPackUnit() {
		return packUnit;
	}

	public void setPackUnit(String packUnit) {
		this.packUnit = packUnit;
	}

	public Boolean getShippingFree() {
		return shippingFree;
	}

	public void setShippingFree(Boolean shippingFree) {
		this.shippingFree = shippingFree;
	}

	public Object getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Object releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getShippingTime() {
		return shippingTime;
	}

	public void setShippingTime(String shippingTime) {
		this.shippingTime = shippingTime;
	}

	public Price[] getPrices() {
		return prices;
	}

	public void setPrices(Price[] prices) {
		this.prices = prices;
	}

	public Attribute getAttribute() {
		return attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public Object[] getConfiguratorOptions() {
		return configuratorOptions;
	}

	public void setConfiguratorOptions(Object[] configuratorOptions) {
		this.configuratorOptions = configuratorOptions;
	}

	public boolean isLastStock() {
		return lastStock;
	}

	public void setLastStock(boolean lastStock) {
		this.lastStock = lastStock;
	}

	public Integer getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(Integer purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	@Override
	public String toString() {
		return "Maindetail [id=" + id + ", articleId=" + articleId + ", unitId=" + unitId + ", number=" + number
				+ ", supplierNumber=" + supplierNumber + ", kind=" + kind + ", additionalText=" + additionalText
				+ ", active=" + active + ", inStock=" + inStock + ", stockMin=" + stockMin + ", weight=" + weight
				+ ", width=" + width + ", len=" + len + ", height=" + height + ", ean=" + ean + ", position=" + position
				+ ", minPurchase=" + minPurchase + ", purchaseSteps=" + purchaseSteps + ", maxPurchase=" + maxPurchase
				+ ", purchaseUnit=" + purchaseUnit + ", referenceUnit=" + referenceUnit + ", packUnit=" + packUnit
				+ ", shippingFree=" + shippingFree + ", releaseDate=" + releaseDate + ", shippingTime=" + shippingTime
				+ ", prices=" + Arrays.toString(prices) + ", attribute=" + attribute + ", configuratorOptions="
				+ Arrays.toString(configuratorOptions) + "]";
	}
}