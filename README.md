# Shopware Scanner Manager
## Restclient for Shopware 5

Current supported Shopware Version: 5.4.5

This Application is providing a native windows executable and jars for a dektop-ui to use the REST-API of an Shopware 5 store to update the stock of an article.
This can be done, by scanning the article-barcode by e.g. an external usb-scanner.

### Functions
The following functions are supported:
 - :+1: incoming (buy) and outgoing (sell) articles
 - :+1: grafical user interface by JavaFX2
 - :+1: history listing of scanned articles
 - :+1: support of articles and variants of articles
 - :+1: article details of scanned products are shown
 - :+1: error-feedback when a problem occurs after entering a code
 - :+1: automatic email-generation for maintainer
 - :+1: buy/sell-statistic as diagram
 
### Gui

![Image of User Interface](gui1.png)

![Image of User Interface](gui2.png)

### Getting started

To run the Application:

	1. clone project
	2. enter authentication data, mails and domain to autheticationData.properties
    3. [optional:] replace icon
	3. install via apache maven (mvn clean install)
	4. run application from executable (.exe) on Windows or from jar-with-dependencies on any plattform, which uses java.
	
Shopware 5 must be installed and the articles, which have to be scanned, need the EAN-Number of the barcode set as Articlenumber and as EAN-Number. This has to be done, because the Shopware 5 Rest API only allows the article-identification by ID and Articlenumber. (not by ean)

### LICENCE

	MIT